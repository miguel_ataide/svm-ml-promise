from seaborn import load_dataset
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import make_column_transformer
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score

# Load the data and create training and testing data
df = pd.read_csv('./data/cm1.csv')
df = df.dropna()
X = df.drop(columns = ['defects', 'id'])
y = df['defects']

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state = 100)

# Create a transformer object
column_transformer = make_column_transformer(
    (OneHotEncoder(), ['total_Opnd', 'branchCount']),
    (StandardScaler(), ['lOCode', 'lOComment', 'lOBlank', 'locCodeAndComment']),
    remainder='passthrough')

data=X_train;
print(data);

# Transform the training features
X_train = column_transformer.fit_transform(X_train)
X_train = pd.DataFrame([data], columns=column_transformer.get_feature_names_out())

# Building and fit the classifier
clf = SVC(kernel='rbf', gamma=0.01, C=1000)
clf.fit(X_train, y_train)

# Transform the training data
X_test = column_transformer.transform(X_test)
X_test = pd.DataFrame(data=X_test, columns=column_transformer.get_feature_names_out())

# Make predictions and check the accuracy
predictions = clf.predict(X_test)
print(accuracy_score(y_test, predictions))

# Returns: 0.9880952380952381