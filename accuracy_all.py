# Importing required libraries
from seaborn import load_dataset, pairplot
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

ALL = 0;
MCCABLE = 1;
HALSTEAD = 2;
OTHER = 3;

all = ['loc', 'v(g)', 'ev(g)', 'iv(g)', 'n', 'v', 'l', 'd', 
                'i','e', 'b', 't', 'lOCode', 'lOComment', 'lOBlank', 
                'locCodeAndComment', 'uniq_Op', 'uniq_Opnd', 'total_Op', 
                'total_Opnd', 'branchCount'];

mccable = ['loc', 'v(g)', 'ev(g)', 'iv(g)'];

halstead = ['n', 'v', 'l', 'd', 'i','e', 'b', 't', 
           'lOCode', 'lOComment', 'lOBlank', 'locCodeAndComment'];

other = ['locCodeAndComment', 'uniq_Op', 'uniq_Opnd', 'total_Op', 
        'total_Opnd', 'branchCount'];

def calc_accuracy(path, columns):

        # Testing the accuracy of our model
        df = pd.read_csv(path)
        df = df.dropna()

        X = df[columns]

        y = df['defects']

        X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=100)

        # print(X_train);

        clf = SVC(kernel='linear')
        clf.fit(X_train, y_train)

        predictions = clf.predict(X_test)
        print(accuracy_score(y_test, predictions))

def run(file, type):
        print("-------------------")
        print(file);

        array = all;

        if(type == MCCABLE):
                array = mccable;
                print("McCable")
        elif(type == HALSTEAD):
                array = halstead;
                print("Halstead")
        elif(type == OTHER):
                array = other;
                print("Other")
        else:
                print("All")

        calc_accuracy(file, array);
        print("-------------------")

file = './data/cm1.csv';

run(file, MCCABLE);
run(file, HALSTEAD);
run(file, OTHER);
run(file, ALL);

file = './data/kc1.csv';

run(file, MCCABLE);
run(file, HALSTEAD);
run(file, OTHER);
run(file, ALL);

file = './data/kc2.csv';

run(file, MCCABLE);
run(file, HALSTEAD);
run(file, OTHER);
run(file, ALL);

file = './data/pc1.csv';

run(file, MCCABLE);
run(file, HALSTEAD);
run(file, OTHER);
run(file, ALL);

