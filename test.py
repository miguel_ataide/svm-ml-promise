# Importing required libraries
from seaborn import load_dataset, pairplot
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Loading and exploring our dataset
df = pd.read_csv('./data/cm1.csv')
print(df.head())

df = df.dropna()
print(len(df))

pairplot(df, hue='defects')
plt.show()

# Splitting our data
X = df[['v(g)', 'n']]
y = df['defects']

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=100)

# The SVC Class from Sklearn
SVC(
    C=1.0,                          # The regularization parameter
    kernel='rbf',                   # The kernel type used 
    degree=3,                       # Degree of polynomial function 
    gamma='scale',                  # The kernel coefficient
    coef0=0.0,                      # If kernel = 'poly'/'sigmoid'
    shrinking=True,                 # To use shrinking heuristic
    probability=False,              # Enable probability estimates
    tol=0.001,                      # Stopping crierion
    cache_size=200,                 # Size of kernel cache
    class_weight=None,              # The weight of each class
    verbose=False,                  # Enable verbose output
    max_iter=- 1,                   # Hard limit on iterations
    decision_function_shape='ovr',  # One-vs-rest or one-vs-one
    break_ties=False,               # How to handle breaking ties
    random_state=None               # Random state of the model
)

# Building and training our model
clf = SVC(kernel='linear')
clf.fit(X_train, y_train)

predictions = clf.predict(X_test)
print(predictions[:10])