# Importing required libraries
from seaborn import load_dataset, pairplot
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Rerunning the algorithm with a binary classifier
df = pd.read_csv('./data/cm1.csv')
df = df.dropna()
# df = df[df['species'] != 'Gentoo']    # This limits us to two classes

# X = df.select_dtypes('number')
X = df[['v(g)', 'n']]
y = df['defects']

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=100)

clf = SVC(kernel='linear')
clf.fit(X_train, y_train)

# Visualizing the linear function for our SVM classifier
import numpy as np
from seaborn import scatterplot
w = clf.coef_[0]
b = clf.intercept_[0]
x_visual = np.linspace(0, 100)
y_visual = np.linspace(0, 2100) #-(w[0] / w[1]) * x_visual - b / w[1]

scatterplot(data = X_train, x='v(g)', y='n', hue=y_train)
plt.plot(x_visual, y_visual)
plt.show()